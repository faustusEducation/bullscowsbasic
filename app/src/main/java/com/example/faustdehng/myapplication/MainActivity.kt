package com.example.faustdehng.myapplication

import android.graphics.Color
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.TooltipCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    /**
     * answer of the game, 4 different digits, leading zero(s) is allowed
     */
    var answer = this.getInit()

    /**
     * Create the main activity
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.btnGo.setOnClickListener  { this.showResult() }
        this.btnAns.setOnClickListener { this.txtResult.text = answer }
        this.btnNew.setOnClickListener {
            this.answer = this.getInit()
            this.txtResult.text = ""
            this.editGuess.setText("")
        }
    }

    /**
     * Get the (random) initial value for a new game
     *
     * 4 different number digits, leading zeros is allowed
     */
    fun getInit() : String {
        var answerDigit: String
        do {
            val random = Random()
            val answer = random.nextInt(10000)
            val digits = "%d".format(answer)
            answerDigit = "0".repeat(4 - digits.length)+digits
        } while(!isValid(answerDigit))
        return answerDigit
    }

    /**
     * check if there are same digits
     *
     * @return true for no same digits, 4 digits are different, false for length wrong or same digits
     */
    private fun isValid(answer: String): Boolean {
        if(answer.length == 4) {
            for(i in 0..2) {
                for(j in (i+1)..3) {
                    if(answer.get(i)==answer.get(j)) {
                        return false
                    }
                }
            }
            return true
        }
        return false
    }

    /**
     * Show the checking result
     */
    fun showResult() {
        val result = MainActivity.checkABs(this.editGuess.text.toString(),this.answer)
        if(result == "4A0B") {
            this.txtResult.setTextColor(Color.parseColor("#006400") )  //RGB code of Dark Green
            this.txtResult.typeface = Typeface.DEFAULT_BOLD
            this.txtResult.text = "Bingo !!"
        } else {
            this.txtResult.setTextColor(Color.RED)
            this.txtResult.typeface = Typeface.DEFAULT
            this.txtResult.text = result
        }
    }

    /**
     * static function or variable (companion object)
     */
    companion object {
        /**
         * Check the anser and the guess, return how many A or B
         *
         * @param   answer  the answer of the game
         * @param   guess   the new guess
         * @return  Pair of (As, Bs), or null for wrong input
         */
        public fun checkABs(answer: String, guess: String): String? {
            if (answer.length != 4 || guess.length != 4) {
                return null
            } else {
                var a: Int = 0
                var b: Int = 0
                for (i in 0..3) {
                    for (j in 0..3) {
                        if (answer.get(i) == guess.get(j)) {
                            if (i == j) a += 1 else {
                                b += 1
                            }
                            break
                        }
                    }
                }
                return a.toString()+"A"+b+"B"
            }
        }
    }
}
